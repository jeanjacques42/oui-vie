import Vue from 'vue'
import Router from 'vue-router'
import Go from './views/Go.vue'
import Home from './views/Home.vue'




Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/settings',
      name: 'settings',
      component: () => import(/* webpackChunkName: "about" */ './views/Settings.vue')
    },
    {
      path: '/go',
      name: 'go',
      component: Go
    }
  ]
})
